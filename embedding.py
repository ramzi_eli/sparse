import os
from glob import glob
from random import shuffle
from pathlib import Path
from shutil import copyfile

import numpy as np
from tqdm import tqdm
from torch.utils.data import DataLoader
from torchvision.transforms import Compose, ToTensor, Resize
from kymatio import Scattering2D

from dataset import ImageDataset


def mount_data(keep=500):
    folders = glob('data/*')
    paths = []
    for fl in folders:
        paths += glob(os.path.join(fl, '*'))[:keep]

    shuffle(paths)
    for idx, pth in enumerate(tqdm(paths, 'Creating files')):
        label = pth.split('/')[-2]
        ext = pth.split('.')[-1]
        new_path = os.path.join("data", f"{idx}_{label}.{ext}")
        copyfile(pth, new_path)


def embed_images(input_shape, batch_size, J, dir_to_save, num_workers=7):
    transform = Compose([
        Resize((input_shape, input_shape)),
        ToTensor()
    ])
    dataset = ImageDataset(transform)
    dataloader = DataLoader(
        dataset,
        batch_size,
        pin_memory=True,
        num_workers=num_workers,
    )
    scattering = Scattering2D(J, (input_shape, input_shape))

    for idx_batch, current_batch in enumerate(tqdm(dataloader)):
        images = current_batch['x'].float().cpu()
        s_images = scattering(images).cpu().numpy()
        s_images = np.reshape(
            s_images,
            (s_images.shape[0], -1, s_images.shape[-1], s_images.shape[-1])
        )

        for idx_local in range(s_images.shape[0]):
            idx_global = idx_local + idx_batch * batch_size
            filename = (
                dir_to_save
                / '{}_{}.npy'.format(
                    idx_global,
                    current_batch['y'][idx_local].split('/')[-1].split('.')[0])
            )
            temp = s_images[idx_local]
            np.save(filename, temp)


if __name__ == '__main__':
    # mount_data(3500)
    # import ipdb; ipdb.set_trace()
    embed_images(
        64,
        300,
        4,
        Path('embedding'),
    )
