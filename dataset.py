import os
from glob import glob

import numpy as np
from PIL import Image
from torch.utils.data import Dataset


class ImageDataset(Dataset):
    def __init__(self, transform, datadir='data', nb_channels=3):
        self.transform = transform
        self.datadir = datadir
        self.nb_files = len(glob(os.path.join(datadir, '*')))
        self.nb_channels = nb_channels

    def pil_loader(self, path):
        with open(path, 'rb') as f:
            img = Image.open(f)
            return img.convert('RGB')

    def __len__(self):
        return self.nb_files

    def __getitem__(self, idx):
        path = glob(os.path.join(self.datadir, f"{idx}_*"))
        assert len(path) == 1
        path = path[0]
        label = path.split('_')[1].split('.')[0]
        if self.nb_channels == 3:
            x = self.transform(self.pil_loader(path)) / 255.0
        else:
            x = self.transform(self.pil_loader(path)) / 255.0

        return {'x': x, 'y': label}


class EmbeddingDataset(Dataset):
    def __init__(self, datadir='embedding'):
        self.datadir = datadir
        self.nb_files = len(glob(os.path.join(datadir, '*')))

    def __len__(self):
        return self.nb_files

    def __getitem__(self, idx):
        path = glob(os.path.join(self.datadir, f"{idx}_*"))
        assert len(path) == 1
        path = path[0]
        label = path.split('_')[1].split('.')[0]
        x = np.load(path)

        return {'x': x, 'y': label}
